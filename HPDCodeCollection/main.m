//
//  main.m
//  HPDCodeCollection
//
//  Created by HePanDai on 14-12-1.
//  Copyright (c) 2014年 HePanDai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
