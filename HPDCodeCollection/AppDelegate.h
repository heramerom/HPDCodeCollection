//
//  AppDelegate.h
//  HPDCodeCollection
//
//  Created by HePanDai on 14-12-1.
//  Copyright (c) 2014年 HePanDai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

